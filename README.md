## Contenedor de Docker-Debian Sistemas Operativos UTP

## Contenedor de Docker-Kubuntu *LAMP*

Contenedor de Lamp usando Docker compose y contiene los siguientes componentes:

* PHP 7.1
* Apache 2.4
* MySQL 5.7
* phpMyAdmin

## Instalación

Clona el repositorio en tu computadora local y cambia a la banch 7.1x, para iniciar la creacion del contenedor docker-compose up -d

Paso a Paso en la consola

```shell
git clone ''
cd docker-compose-lamp/
git fetch --all
git checkout 7.1.x
docker-compose up -d
```

## Listar las maquinas del contenedor
```shell
docker ps
```

## Entrar a una maquina del contenedor
```shell
docker exec -i -t *Numero del contenedor* /bin/bash
```

## Apagar el contenedor y las maquinas
```shell
docker-compose stop
```

## Creado por Alberto Muñoz Gitlab: @betoo0217
## Creado por Carlos Vaccaro Gitlab: @cafavaro